console.log(getPromoCode(85798887))

function getPromoCode(promoCode) {
    if (typeof promoCode !== 'number') {
        alert('Not at number')

        return
    }

    let promoCodeData = promoCode.toString().split('')

    if (promoCodeData.length !== 8) {
        alert('Not valid promo code. Need 8 symbols!')

        return
    }

    if (checkHighPriority(promoCodeData)) {
        return 2000
    }

    if (checkMiddlePriority(promoCodeData)) {
        return 1000
    }

    if (checkLowPriority(promoCodeData)) {
        return 100
    }

    return 0
}

function checkHighPriority(promoCodeData) {
    let first = []
    let firstIndices = []
    let last = []
    let lastIndices = []
    let skipOnce = false

    for (let i = 0; i < promoCodeData.length; i++) {

        if (skipOnce) {
            skipOnce = false

            continue
        }

        if (promoCodeData[i] % 2 === 0) {
            continue
        }

        if (first.length < 2) {
            firstIndices.push(i)
            if ((firstIndices[0] + 1 === i && promoCodeData[firstIndices[0]] !== promoCodeData[i]) || first.length === 0) {
                first.push(promoCodeData[i])
            }

            if (first.length === 2) {
                skipOnce = true
            }
            continue
        }
        if (last.length < 2) {
            lastIndices.push(i)
            if ((lastIndices[0] + 1 === i && promoCodeData[lastIndices[0]] !== promoCodeData[i]) || last.length === 0) {
                last.push(promoCodeData[i])
            }
        }
    }

    if (first.length === 2 && last.length === 2) {
        return ascending(first) && ascending(last)
    }

    return false
}

function checkMiddlePriority(promoCodeData) {
    let first = []
    let firstIndices = []
    let last = []
    let lastIndices = []
    let skipOnce = false

    for (let i = 0; i < promoCodeData.length; i++) {

        if (skipOnce) {
            skipOnce = false

            continue
        }

        if (promoCodeData[i] % 2 === 0) {
            continue
        }

        if (first.length < 2) {
            firstIndices.push(i)
            if ((firstIndices[0] + 1 === i && promoCodeData[firstIndices[0]] !== promoCodeData[i]) || first.length === 0) {
                first.push(promoCodeData[i])
            }

            if (first.length === 2) {
                skipOnce = true
            }
            continue
        }
        if (last.length < 2) {
            lastIndices.push(i)
            if ((lastIndices[0] + 1 === i && promoCodeData[lastIndices[0]] !== promoCodeData[i]) || last.length === 0) {
                last.push(promoCodeData[i])
            }
        }
    }


    if (first.length === 2 && last.length === 2) {
        return true
    }

    return false
}

function ascending(data) {
    return data[0] < data[1]
}

function checkLowPriority(promoCodeData) {
    let oddNumber = 0
    let evenNumber = 0

    promoCodeData.forEach(item => {
        if (item % 2 === 0) {
            evenNumber += +item

            return
        }

        oddNumber += +item
    })

    return evenNumber > oddNumber
}